// Description:
//   Inspired by Lucky from King of the Hill
//
// Dependencies:
//   None
//
// Configuration:
//   None
//
// Commands:
//   lucky say something - returns a random lucky quote
//   lucky random - returns a link to a random King of the Hill fandom wiki article
//   lucky pocket sand - returns an effective defense (admittedly Dale, not Lucky)

module.exports = function(robot) {
 
  robot.respond(/say something/, function(res) {
    var luckyIcon = 'https://imgur.com/Xag34iE.png';
    var luckyQuotes = [
      "She carries a strong resemblance to my side of the family.. though that may change once she gets teeth",
      "I slipped on pee-pee at the Costco.",
      "Nothing like seeing your garbage laid out to make you realize how fortunate you are",
      "Me, I don’t go to church. Church goes with me. I’m worshipping when I’m drinking a beer, digging a hole, or fishing for trout.",
      "Sorry but I don't do hospitals. Everyone I know that's died was shot in the woods and taken to a hospital.....Where they died.",
      "Eatin' a chip off the line is one of those experiences that changes your life. Birds fly a little slower, and pretty girls smile a little longer. It's like the whole world moves just for you.",
      "Lucky is on the case. That's usually what I say when I drink beer, but this time it refers to this mystery.",
      "Dad,were you in the state oil rig or in the federal?",
      "Cigarette math is full of surprises.",
      "I've seen a lot of nature documentaries and I find that most of life's questions can be answered with another question: what would a monkey do?",
      "it's not a pill, but I make something in my bathtub that'll make you happy",
      "there are plenty of times I've wished I had a device to hold me upright in the bathtub "
    ];
    res.send(res.random(luckyQuotes));
  });

  robot.respond(/random/, function(res) {
    var randomWikiUrl = 'https://kingofthehill.fandom.com/wiki/Special:Random';
    robot.http(randomWikiUrl).get()(function(err, response, body) {
      res.send(response.headers['location']);
    });
  });

  robot.hear(/(pocket sand|sha sha sha)/, function(res) {
    var pocketSandGif = 'https://giphy.com/gifs/i2GADdaJIscPS';
    res.send(pocketSandGif);
    robot.adapter.client.web.chat.postMessage(res.message.room, 'pocket sand!', {
      as_user: true,
      attachments: [
        {
          fallback: "gif of man throwing sand from his pocket",
          image_url: pocketSandGif
        }
      ]
    });
  });

  robot.respond(/regex \/(.*)\/ (.*)/, function(res) {
    var re = new RegExp(res.match[1]);
    var result = re.exec(res.match[2]);
    if (result) {
      var matches = result.slice(1, result.length);
      var highlightedMatches = res.match[2];
      for ( var i = 0; i < matches.length; i++ ) {
        // '\u200B' is a "zero-width space" used to cheat markdown for inline bolding
        // TODO: this highlight logic is not perfect
        highlightedMatches = highlightedMatches.replace(matches[i], '\u200B*' + matches[i] + '*\u200B');
      }
      res.send(highlightedMatches);
    } else {
      res.send('no matches');
    }
  });

}
